package me.tanaphat.inv;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Ranjug implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			Player randomPlayer = Bukkit.getOnlinePlayers().stream().findAny().get();

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -277, 72, 225);

			randomPlayer.teleport(back);
			randomPlayer.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			randomPlayer.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 99999, 0));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(0, 255, 255));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(0, 255, 255));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack hand = new ItemStack(Material.TRIDENT);
			hand.addEnchantment(Enchantment.LOYALTY, 1);
			ItemMeta le = hand.getItemMeta();
			le.setUnbreakable(true);
			hand.setItemMeta(le);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			randomPlayer.getInventory().setHelmet(head);
			randomPlayer.getInventory().setChestplate(body);

			randomPlayer.getInventory().addItem(hand, eat);
		}
		return true;
	}


	
		


}
