package me.tanaphat.inv;

import java.util.Arrays;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KillReward implements Listener {

	Random rand = new Random();
	
	@EventHandler
	public void food(PlayerDeathEvent d) {
		Player p = d.getEntity();
		Player killer = p.getKiller();
		if(killer != p) {
			p.sendMessage("you killed yourself");
		}
		else killer.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
	}
	
	
	@EventHandler
	public void reward(PlayerDeathEvent d) {
		Player p = d.getEntity();
		Player killer = p.getKiller();
		if(killer != p) {
			p.sendMessage("you killed your self");
		}
			else if (Perk.Perk.get(killer).equals("None")) {
			if (killer instanceof Player) {
				if (Menu.seleceted.get(killer).equals("Flagellant")) {
					ItemStack re = new ItemStack(Material.GRAY_DYE, 1);
					ItemMeta meleem = re.getItemMeta();
					meleem.setDisplayName("REPLENISH");
					meleem.setLore(Arrays.asList("Right click to sacrifice your hunger for skill"));
					re.setItemMeta(meleem);
					killer.getInventory().addItem(re);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Goat")) {
					ItemStack hand2 = new ItemStack(Material.WOODEN_SWORD);
					hand2.addUnsafeEnchantment(Enchantment.KNOCKBACK, 12);
					ItemMeta meleemeta2 = hand2.getItemMeta();
					Damageable damageable = (Damageable) meleemeta2;
					meleemeta2.setDisplayName(ChatColor.GREEN + "GOOGLE CHROME");
					damageable.setDamage(58);
					hand2.setItemMeta(meleemeta2);
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					killer.getInventory().addItem(hand2);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Archer")) {
					killer.getInventory().addItem(new ItemStack(Material.ARROW, 6));
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					return;

				}
				if (Menu.seleceted.get(killer).equals("Blaze")) {
					killer.getInventory().addItem(new ItemStack(Material.ARROW, 3));
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					return;
				}

				if (Menu.seleceted.get(killer).equals("Executioner")) {
					ItemStack sus = new ItemStack(Material.PLAYER_HEAD, 1);
					ItemMeta su = sus.getItemMeta();
					su.setDisplayName(ChatColor.RED + "Head");
					su.setLore(Arrays.asList("Right click to gain haste and protection for a short time"));
					sus.setItemMeta(su);
					killer.getInventory().addItem(sus);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Alchemist")) {
					ItemStack box = new ItemStack(Material.CHEST, 2);
					ItemMeta bo = box.getItemMeta();
					bo.setDisplayName(ChatColor.RED + "ingredients box");
					bo.setLore(Arrays.asList("Right click to recieve a random ingredient for crafting"));
					box.setItemMeta(bo);
					killer.getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE));
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					killer.getInventory().addItem(box);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Assassin")) {
					ItemStack sharddd = new ItemStack(Material.YELLOW_DYE);
					ItemMeta shard = sharddd.getItemMeta();
					shard.setDisplayName("Swords shard");
					shard.setLore(Arrays.asList("an ingredient use to craft a new orphan obliterator"));
					sharddd.setItemMeta(shard);
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					killer.getInventory().addItem(sharddd);
					return;

				} else {
					killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
					return;
				}
			} else if (Perk.Perk.get(killer).equalsIgnoreCase("Golden Head")) {
				if (killer instanceof Player)

					if (Menu.seleceted.get(killer).equals("Flagellant")) {
						ItemStack re = new ItemStack(Material.GRAY_DYE, 1);
						ItemMeta meleem = re.getItemMeta();
						meleem.setDisplayName("REPLENISH");
						meleem.setLore(Arrays.asList("Right click to sacrifice your hunger for skill"));
						re.setItemMeta(meleem);
						killer.getInventory().addItem(re);
						return;
					}
				if (Menu.seleceted.get(killer).equals("Goat")) {
					ItemStack hand2 = new ItemStack(Material.WOODEN_SWORD);
					hand2.addUnsafeEnchantment(Enchantment.KNOCKBACK, 12);
					ItemMeta meleemeta2 = hand2.getItemMeta();
					meleemeta2.setDisplayName(ChatColor.GREEN + "GOOGLE CHROME");
					hand2.setItemMeta(meleemeta2);
					hand2.setDurability((short) 58);
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);

					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					killer.getInventory().addItem(hand2, GAP);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Archer")) {
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);
					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					killer.getInventory().addItem(new ItemStack(Material.ARROW, 6));
					killer.getInventory().addItem(GAP);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Blaze")) {
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);
					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					killer.getInventory().addItem(new ItemStack(Material.ARROW, 3));
					killer.getInventory().addItem(GAP);
					return;
				}

				if (Menu.seleceted.get(killer).equals("Executioner")) {
					ItemStack sus = new ItemStack(Material.PLAYER_HEAD, 1);
					ItemMeta su = sus.getItemMeta();
					su.setLore(Arrays.asList("Right click to gain haste and protection for a short time"));
					su.setDisplayName(ChatColor.RED + "Head");
					sus.setItemMeta(su);
					killer.getInventory().addItem(sus);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Alchemist")) {
					ItemStack box = new ItemStack(Material.CHEST, 2);
					ItemMeta bo = box.getItemMeta();
					bo.setDisplayName(ChatColor.RED + "ingredients box");
					bo.setLore(Arrays.asList("Right click to recieve a random ingredient for crafting"));
					box.setItemMeta(bo);
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);
					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					killer.getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE));
					killer.getInventory().addItem(box, GAP);
					return;
				}
				if (Menu.seleceted.get(killer).equals("Assassin")) {
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);
					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					ItemStack sharddd = new ItemStack(Material.YELLOW_DYE);
					ItemMeta shard = sharddd.getItemMeta();
					shard.setDisplayName("Swords shard");
					shard.setLore(Arrays.asList("an ingredient use to craft a new orphan obliterator"));
					sharddd.setItemMeta(shard);
					killer.getInventory().addItem(GAP, sharddd);
					return;
				}

				else {
					ItemStack GAP = new ItemStack(Material.GOLD_INGOT);
					ItemMeta GAD = GAP.getItemMeta();
					GAD.setDisplayName("Golden Head");
					GAP.setItemMeta(GAD);
					killer.getInventory().addItem(GAP);
					return;
				}
			} else {
				if (killer instanceof Player)
					if (Perk.Perk.get(killer).equals("Streaker")) {
						killer.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 160, 6));
						return;
					}

			}
		}
	}
	
}
