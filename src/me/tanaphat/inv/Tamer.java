package me.tanaphat.inv;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Tamer implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -262, 71, 177);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 9999999, 2));

			player.teleport(back);

			// item here
			ItemStack head = new ItemStack(Material.IRON_HELMET);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			ItemStack hand = new ItemStack(Material.WOODEN_SHOVEL);
			hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 6);
			ItemMeta meleemeta = hand.getItemMeta();
			meleemeta.setDisplayName(ChatColor.YELLOW + "ANTI HORNY STICK");
			hand.setItemMeta(meleemeta);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 16);
			ItemStack spawn = new ItemStack(Material.WOLF_SPAWN_EGG, 6);
			ItemMeta sm = spawn.getItemMeta();
			sm.setDisplayName(ChatColor.GRAY + "Spawn Hond!");
			spawn.setItemMeta(sm);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat, spawn);
		}
		return true;
	}

}
