package me.tanaphat.inv;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import Passive.Ace_P;
import Passive.Archer_P;
import Passive.Assassin_P;
import Passive.Blaze_P;
import Passive.Cat_P;
import Passive.Executioner_P;
import Passive.Flagellant_P;
import Passive.Shark_P;
import Passive.Warden_P;
import me.tanaphat.clas.Ace;
import me.tanaphat.clas.Alchemist;
import me.tanaphat.clas.Archer;
import me.tanaphat.clas.Assassin;
import me.tanaphat.clas.Blaze;
import me.tanaphat.clas.Bulwark;
import me.tanaphat.clas.Cactus;
import me.tanaphat.clas.Cat;
import me.tanaphat.clas.Confuser;
import me.tanaphat.clas.Paladin;
import me.tanaphat.clas.Shark;
import me.tanaphat.clas.Executioner;
import me.tanaphat.clas.Flagellant;
import me.tanaphat.clas.Goat;
import me.tanaphat.clas.Impaler;
import me.tanaphat.clas.Juggernaut;
import me.tanaphat.clas.Stalker;
import me.tanaphat.clas.Warden;
import me.tanaphat.clas.Warrior;
import me.tanaphat.skill.Alchemist_box;
import me.tanaphat.skill.Blaze_S;
import me.tanaphat.skill.Confuser_S;
import me.tanaphat.skill.Executioner_S;
import me.tanaphat.skill.Flagallant_S;
import me.tanaphat.skill.GAP;
import me.tanaphat.skill.Paladin_S;
import me.tanaphat.skill.Stalker_S;
import me.tanaphat.skill.Warden_S;

public class Main extends JavaPlugin {

	public void onEnable() {
		getServer().addRecipe(harmming());
		getServer().addRecipe(poison());
		getServer().addRecipe(weakness());
		getServer().addRecipe(slowness());
		getServer().addRecipe(heal());
		getServer().addRecipe(speed());
		getServer().addRecipe(sword());

		Bukkit.getServer().getPluginManager().registerEvents(new Testsomething(this), this);
		this.getCommand("kit_Warrior").setExecutor(new Warrior());
		this.getCommand("kit_Archer").setExecutor(new Archer());
		this.getCommand("kit_Executioner").setExecutor(new Executioner());
		this.getCommand("kit_Bulwark").setExecutor(new Bulwark());
		this.getCommand("kit_Cactus").setExecutor(new Cactus());
		this.getCommand("kit_Stalker").setExecutor(new Stalker());
		this.getCommand("kit_Impaler").setExecutor(new Impaler());
		this.getCommand("kit_Alchemist").setExecutor(new Alchemist());
		this.getCommand("kit_Warden").setExecutor(new Warden());
		this.getCommand("kit_Blaze").setExecutor(new Blaze());
		this.getCommand("kit_Cat").setExecutor(new Cat());
		this.getCommand("kit_Goat").setExecutor(new Goat());
		this.getCommand("kit_Shark").setExecutor(new Shark());
		this.getCommand("kit_Assassin").setExecutor(new Assassin());
		this.getCommand("kit_Tamer").setExecutor(new Tamer());
		this.getCommand("kit_Flagellant").setExecutor(new Flagellant());
		this.getCommand("kit_Ace").setExecutor(new Ace());
		this.getCommand("kit_Paladin").setExecutor(new Paladin());
		this.getCommand("kit_Confuser").setExecutor(new Confuser());
		this.getCommand("kit_jug").setExecutor(new Juggernaut());

		this.getCommand("Class").setExecutor(new Gui());
		this.getCommand("Perk").setExecutor(new Perk_Gui());

		getServer().getPluginManager().registerEvents(new Menu(), this);
		getServer().getPluginManager().registerEvents(new Sign(), this);
		getServer().getPluginManager().registerEvents(new Flagallant_S(), this);
		getServer().getPluginManager().registerEvents(new Paladin_S(), this);
		getServer().getPluginManager().registerEvents(new Blaze_S(), this);
		getServer().getPluginManager().registerEvents(new Confuser_S(), this);
		getServer().getPluginManager().registerEvents(new Stalker_S(), this);
		getServer().getPluginManager().registerEvents(new Executioner_S(), this);
		getServer().getPluginManager().registerEvents(new KillReward(), this);
		getServer().getPluginManager().registerEvents(new GAP(), this);
		getServer().getPluginManager().registerEvents(new Perk(), this);
		getServer().getPluginManager().registerEvents(new Alchemist_box(), this);
		getServer().getPluginManager().registerEvents(new Warden_S(), this);
		
		getServer().getPluginManager().registerEvents(new Shark_P(), this);
		getServer().getPluginManager().registerEvents(new Blaze_P(), this);
		getServer().getPluginManager().registerEvents(new Ace_P(), this);
		getServer().getPluginManager().registerEvents(new Archer_P(), this);
		getServer().getPluginManager().registerEvents(new Cat_P(), this);
		getServer().getPluginManager().registerEvents(new Executioner_P(), this);
		getServer().getPluginManager().registerEvents(new Warden_P(), this);
		getServer().getPluginManager().registerEvents(new Assassin_P(), this);
		getServer().getPluginManager().registerEvents(new Flagellant_P(), this);
		getServer().getPluginManager().registerEvents(new Launchpad(), this);
		getServer().getPluginManager().registerEvents(new Teleport(), this);
	}

	public ShapedRecipe harmming() {

		ItemStack bottle = new ItemStack(Material.SPLASH_POTION, 3);
		PotionMeta pm = (PotionMeta) bottle.getItemMeta();
		pm.setMainEffect(PotionEffectType.HARM);
		PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 20, 0);
		pm.addCustomEffect(harm, true);
		pm.setDisplayName("�4HARMING POTION");
		bottle.setItemMeta(pm);

		ShapedRecipe harmming = new ShapedRecipe(new NamespacedKey(this, "bottle"), bottle);

		harmming.shape("e", "b");
		harmming.setIngredient('b', Material.GLASS_BOTTLE);
		harmming.setIngredient('e', Material.GLISTERING_MELON_SLICE);

		return harmming;
	}

	public ShapedRecipe poison() {

		ItemStack p2 = new ItemStack(Material.SPLASH_POTION, 3);
		PotionMeta pm2 = (PotionMeta) p2.getItemMeta();
		pm2.setMainEffect(PotionEffectType.POISON);
		PotionEffect poi = new PotionEffect(PotionEffectType.POISON, 200, 2);
		pm2.addCustomEffect(poi, true);
		pm2.setDisplayName("�2POISION POTION");
		p2.setItemMeta(pm2);

		ShapedRecipe poison = new ShapedRecipe(new NamespacedKey(this, "p2"), p2);

		poison.shape("d", "b");
		poison.setIngredient('b', Material.GLASS_BOTTLE);
		poison.setIngredient('d', Material.SPIDER_EYE);

		return poison;
	}

	public ShapedRecipe weakness() {

		ItemStack p3 = new ItemStack(Material.SPLASH_POTION, 3);
		PotionMeta pm3 = (PotionMeta) p3.getItemMeta();
		pm3.setMainEffect(PotionEffectType.WEAKNESS);
		PotionEffect weak = new PotionEffect(PotionEffectType.WEAKNESS, 200, 2);
		pm3.addCustomEffect(weak, true);
		pm3.setDisplayName("�dWEAKNESS POTION");
		p3.setItemMeta(pm3);

		ShapedRecipe weakness = new ShapedRecipe(new NamespacedKey(this, "p3"), p3);

		weakness.shape("d", "b");
		weakness.setIngredient('b', Material.GLASS_BOTTLE);
		weakness.setIngredient('d', Material.FERMENTED_SPIDER_EYE);
		
		return weakness;
	}
	
	public ShapedRecipe slowness() {
		
		ItemStack p4 = new ItemStack(Material.SPLASH_POTION, 2);
		PotionMeta pm4 = (PotionMeta) p4.getItemMeta();
		pm4.setMainEffect(PotionEffectType.SLOW);
		PotionEffect slow = new PotionEffect(PotionEffectType.SLOW, 275, 3);
		pm4.addCustomEffect(slow, true);
		pm4.setDisplayName("�9SLOWNESS POTION");
		p4.setItemMeta(pm4);
		
		ShapedRecipe slowness = new ShapedRecipe(new NamespacedKey(this, "p4"), p4);

		slowness.shape("d", "b");
		slowness.setIngredient('b', Material.GLASS_BOTTLE);
		slowness.setIngredient('d', Material.RABBIT_FOOT);
		
		return slowness;
	}
	public ShapedRecipe heal() {
		
		ItemStack p5 = new ItemStack(Material.POTION, 1);
		PotionMeta pm5 = (PotionMeta) p5.getItemMeta();
		pm5.setMainEffect(PotionEffectType.HEAL);
		PotionEffect ins_h = new PotionEffect(PotionEffectType.HEAL, 1, 0);
		pm5.addCustomEffect(ins_h, true);
		pm5.setDisplayName("�cINSTA HEAL POTION");
		p5.setItemMeta(pm5);
		
		ShapedRecipe heal = new ShapedRecipe(new NamespacedKey(this, "p5"), p5);

		heal.shape("d", "b");
		heal.setIngredient('b', Material.GLASS_BOTTLE);
		heal.setIngredient('d', Material.GHAST_TEAR);
		
		return heal;
	}
	public ShapedRecipe speed() {
		
		ItemStack p6 = new ItemStack(Material.POTION, 1);
		PotionMeta pm6 = (PotionMeta) p6.getItemMeta();
		pm6.setMainEffect(PotionEffectType.SPEED);
		PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, 160, 2);
		pm6.addCustomEffect(speed, true);
		pm6.setDisplayName("�bSPEED POTION");
		p6.setItemMeta(pm6);
		
		ShapedRecipe runner = new ShapedRecipe(new NamespacedKey(this, "p6"), p6);

		runner.shape("d", "b");
		runner.setIngredient('b', Material.GLASS_BOTTLE);
		runner.setIngredient('d', Material.SUGAR);
		
		return runner;
	}
	public ShapedRecipe sword() {
		
		AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", 10, Operation.ADD_NUMBER);
		AttributeModifier speed = new AttributeModifier(UUID.randomUUID(), "generic.attackSpeed", -3, Operation.ADD_NUMBER);
		ItemStack Ani = new ItemStack(Material.GOLDEN_SWORD);
		ItemMeta meleemeta = Ani.getItemMeta();
		meleemeta.setDisplayName(ChatColor.BLUE + "Orphan Obliterator");
		meleemeta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
		meleemeta.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, speed);
		Ani.setItemMeta(meleemeta);
		Ani.setDurability((short) 31);
		
		Material sharddd = Material.YELLOW_DYE;
        int ammount = 1;
        String name = "Sword shard";
		
		ShapedRecipe obi = new ShapedRecipe(new NamespacedKey(this, "ophran"), Ani);

		obi.shape("a","a");
		obi.setIngredient('a', sharddd);
		
		return obi;
	}
	
}