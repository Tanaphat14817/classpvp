package me.tanaphat.inv;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gui implements CommandExecutor{
	
	@Override
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			
			Inventory gui = Bukkit.createInventory(p, 18,"Class");
			ItemStack item1 = new ItemStack(Material.STONE_SWORD, 1);
			ItemMeta item1_meta = item1.getItemMeta();
			item1_meta.setLore(Arrays.asList(ChatColor.WHITE + "A basic starter class"));
			item1_meta.setDisplayName("Warrior");
			item1.setItemMeta(item1_meta);
			
			ItemStack item2 = new ItemStack(Material.BOW, 1);
			ItemMeta item2_meta = item2.getItemMeta();
			item2_meta.setLore(Arrays.asList(ChatColor.GREEN + "Take out your enemy from afar"));
			item2_meta.setDisplayName("Archer");
			item2.setItemMeta(item2_meta);
			
			ItemStack item3 = new ItemStack(Material.BLAZE_ROD, 1);
			ItemMeta item3_meta = item3.getItemMeta();
			item3_meta.setLore(Arrays.asList(ChatColor.YELLOW + "A taste from hell"));
			item3_meta.setDisplayName("Blaze");
			item3.setItemMeta(item3_meta);
			
			ItemStack item4 = new ItemStack(Material.SHIELD, 1);
			ItemMeta item4_meta = item4.getItemMeta();
			item4_meta.setLore(Arrays.asList(ChatColor.GRAY + "All mighty"));
			item4_meta.setDisplayName("Bulwark");
			item4.setItemMeta(item4_meta);
			
			ItemStack item5 = new ItemStack(Material.CACTUS, 1);
			ItemMeta item5_meta = item5.getItemMeta();
			item5_meta.setLore(Arrays.asList(ChatColor.DARK_GREEN + "Thorn of death"));
			item5_meta.setDisplayName("Cactus");
			item5.setItemMeta(item5_meta);
			
			ItemStack item6 = new ItemStack(Material.COOKED_COD, 1);
			ItemMeta item6_meta = item6.getItemMeta();
			item6_meta.setLore(Arrays.asList(ChatColor.GOLD + "Meow ^�w�^"));
			item6_meta.setDisplayName("Cat");
			item6.setItemMeta(item6_meta);
			
			ItemStack item7 = new ItemStack(Material.IRON_AXE, 1);
			ItemMeta item7_meta = item7.getItemMeta();
			item7_meta.setLore(Arrays.asList(ChatColor.STRIKETHROUGH + "The head cutter"));
			item7_meta.setDisplayName("Executioner");
			item7.setItemMeta(item7_meta);
			
			ItemStack item8 = new ItemStack(Material.TRIDENT, 1);
			ItemMeta item8_meta = item8.getItemMeta();
			item8_meta.setLore(Arrays.asList(ChatColor.BLUE + "From the sea"));
			item8_meta.setDisplayName("Impaler");
			item8.setItemMeta(item8_meta);
			
			ItemStack item9 = new ItemStack(Material.GRASS, 1);
			ItemMeta item9_meta = item9.getItemMeta();
			item9_meta.setLore(Arrays.asList(ChatColor.GRAY + "Stalk down your prey"));
			item9_meta.setDisplayName("Stalker");
			item9.setItemMeta(item9_meta);
			
			ItemStack item10 = new ItemStack(Material.NETHERITE_CHESTPLATE, 1);
			ItemMeta item10_meta = item10.getItemMeta();
			item10_meta.setLore(Arrays.asList(ChatColor.AQUA + "Demon of the depth"));
			item10_meta.setDisplayName("Warden");
			item10.setItemMeta(item10_meta);
			
			ItemStack item11 = new ItemStack(Material.POTION, 1);
			ItemMeta item11_meta = item11.getItemMeta();
			item11_meta.setLore(Arrays.asList(ChatColor.ITALIC + "The potion master"));
			item11_meta.setDisplayName("Alchemist");
			item11.setItemMeta(item11_meta);
			
			ItemStack item12 = new ItemStack(Material.DIORITE, 1);
			ItemMeta item12_meta = item12.getItemMeta();
			item12_meta.setLore(Arrays.asList(ChatColor.GRAY + "FUS RO DAH!!!"));
			item12_meta.setDisplayName("Goat");
			item12.setItemMeta(item12_meta);
			
			ItemStack item13 = new ItemStack(Material.IRON_NUGGET, 1);
			ItemMeta item13_meta = item12.getItemMeta();
			item13_meta.setLore(Arrays.asList(ChatColor.DARK_BLUE + "A"));
			item13_meta.setDisplayName("Shark");
			item13.setItemMeta(item13_meta);
			
			ItemStack item14 = new ItemStack(Material.GOLDEN_SWORD, 1);
			ItemMeta item14_meta = item14.getItemMeta();
			item14_meta.setLore(Arrays.asList(ChatColor.RED + "The finisher"));
			item14_meta.setDisplayName("Assassin");
			item14.setItemMeta(item14_meta);
			
			ItemStack item15 = new ItemStack(Material.LEAD, 1);
			ItemMeta item15_meta = item14.getItemMeta();
			item15_meta.setLore(Arrays.asList(ChatColor.DARK_RED + "PAIN!!! more PAIN!!!!"));
			item15_meta.setDisplayName("Flagellant");
			item15.setItemMeta(item15_meta);
			
			ItemStack item16 = new ItemStack(Material.ELYTRA, 1);
			ItemMeta item16_meta = item16.getItemMeta();
			item16_meta.setLore(Arrays.asList(ChatColor.WHITE + "Kamikaze!!!!!"));
			item16_meta.setDisplayName("Ace");
			item16.setItemMeta(item16_meta);
			
			ItemStack item17 = new ItemStack(Material.YELLOW_BANNER, 1);
			ItemMeta item17_meta = item16.getItemMeta();
			item17_meta.setLore(Arrays.asList(ChatColor.BOLD + "Banners of hope"));
			item17_meta.setDisplayName("Paladin");
			item17.setItemMeta(item17_meta);
			
			ItemStack item18 = new ItemStack(Material.SUSPICIOUS_STEW, 1);
			ItemMeta item18_meta = item16.getItemMeta();
			item18_meta.setLore(Arrays.asList("Random effect go!"));
			item18_meta.setDisplayName("Confuser");
			item18.setItemMeta(item18_meta);
			
			gui.addItem(item1);
			gui.addItem(item2);
			gui.addItem(item3);
			gui.addItem(item4);
			gui.addItem(item5);
			gui.addItem(item6);
			gui.addItem(item7);
			gui.addItem(item8);
			gui.addItem(item9);
			gui.addItem(item10);
			gui.addItem(item11);
			gui.addItem(item12);
			gui.addItem(item13);
			gui.addItem(item14);
			gui.addItem(item15);
			gui.addItem(item16);
			gui.addItem(item17);
			gui.addItem(item18);
			
			p.openInventory(gui);

			
		}
		return true;
}
}
