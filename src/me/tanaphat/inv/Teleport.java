package me.tanaphat.inv;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Teleport implements Listener{
	
	@EventHandler
	public void Plains(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block clickedblock = e.getClickedBlock();
			if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 244) {
				
				Location back = new Location(p.getWorld(), -274, 71, 163);

				p.teleport(back);
				p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
			}

		}

}
	@EventHandler
	public void Nether(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block clickedblock = e.getClickedBlock();
			if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 244) {
				
				Location back = new Location(p.getWorld(), -268, 78, 138);

				p.teleport(back);
				p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
			}

		}
	}
		@EventHandler
		public void Desert(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 246) {
					
					Location back = new Location(p.getWorld(), -252, 74, 213);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
				}

			}

}
		@EventHandler
		public void Forest(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 246) {
					
					Location back = new Location(p.getWorld(), -287, 80, 198);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
				}

			}

}
		@EventHandler
		public void Mesa(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 248) {
					
					Location back = new Location(p.getWorld(), -257, 78, 247);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
				}
			}

			}
		@EventHandler
		public void Ocean(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -374 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -293, 68, 226);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}

}
		@EventHandler
		public void Swamp(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -376 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -328, 71, 221);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
			
}
		@EventHandler
		public void Savana(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -376 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -298, 72, 247);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}

}
		@EventHandler
		public void Mushroom(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -378 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -343, 74, 259);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}

}
		@EventHandler
		public void Taiga(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -378 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -361, 72, 199);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}

}
		@EventHandler
		public void Ruins(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -390, 85, 215);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
		@EventHandler
		public void End(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 248) {
		
					Location back = new Location(p.getWorld(), -374, 82, 244);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
		@EventHandler
		public void Mega(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 246) {
		
					Location back = new Location(p.getWorld(), -365, 97, 170);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
		@EventHandler
		public void Snow(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 246) {
		
					Location back = new Location(p.getWorld(), -378, 74, 149);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
		@EventHandler
		public void Jungle(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 122 && clickedblock.getLocation().getZ() == 244) {
		
					Location back = new Location(p.getWorld(), -325, 102, 166);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
		@EventHandler
		public void Mountain(PlayerInteractEvent e) {
			Player p = e.getPlayer();
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block clickedblock = e.getClickedBlock();
				if (clickedblock.getLocation().getX() == -380 && clickedblock.getLocation().getY() == 121 && clickedblock.getLocation().getZ() == 244) {
		
					Location back = new Location(p.getWorld(), -324, 86, 136);

					p.teleport(back);
					p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
					}

				}
		}
}
