package me.tanaphat.inv;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class Perk implements Listener {
public static HashMap<Player, String> Perk = new HashMap<Player, String>();
	
@EventHandler
public void onjoin(PlayerJoinEvent j) {
	Player p = j.getPlayer();
	Perk.put(p, "None");
}

@EventHandler
public void onMeuclick(InventoryClickEvent e) {
	if (e.getView().getTitle().equalsIgnoreCase("Perk")) {
		Player p = (Player) e.getWhoClicked();
		if (e.getSlot() == 0) {
			Perk.put(p, "Golden Head");
			p.sendMessage(ChatColor.WHITE + "Your current perk is Golden Head");
			p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
		} else if (e.getSlot() == 1) {
			Perk.put(p, "Streaker");
			p.sendMessage(ChatColor.WHITE + "Your current perk is Kill Streaker");
			p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
		} else if (e.getSlot() == 2) {
			Perk.put(p, "None");
			p.sendMessage(ChatColor.WHITE + "Your current perk is None");
			p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
		} else {
			p.closeInventory();
		}
		e.setCancelled(true);
	}
}
}
