package me.tanaphat.inv;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Menu implements Listener {

	public static HashMap<Player, String> seleceted = new HashMap<Player, String>();
	
	//Warrior line 97
	//Archer line 136
	@EventHandler
	public void onjoin(PlayerJoinEvent j) {
		Player p = j.getPlayer();
		seleceted.put(p, "join");
	}
	
	@EventHandler
	public void onMeuclick(InventoryClickEvent e) {
		if (e.getView().getTitle().equalsIgnoreCase("Class")) {
			Player p = (Player) e.getWhoClicked();
				if (e.getSlot() == 0) {
					LoadWarrior(p);
					seleceted.put(p, "Warrior");
				}
				if (e.getSlot() == 1) {
					LoadArcher(p);
					seleceted.put(p, "Archer");
				}
				if (e.getSlot() == 2) {
					LoadBlaze(p);
					seleceted.put(p, "Blaze");
				}
				if (e.getSlot() == 3) {
					LoadBulwark(p);
					seleceted.put(p, "Bulwark");
				}
				if (e.getSlot() == 4) {
					Loadcactus(p);
					seleceted.put(p, "Cactus");
				}
				if (e.getSlot() == 5) {
					Loadcat(p);
					seleceted.put(p, "Cat");
				}
				if (e.getSlot() == 6) {
					LoadExecutioner(p);
					seleceted.put(p, "Executioner");
				}
				if (e.getSlot() == 7) {
					Loadimpaler(p);
					seleceted.put(p, "Impaler");
				}
				if (e.getSlot() == 8) {
					Loadstalker(p);
					seleceted.put(p, "Stalker");
				}
				if (e.getSlot() == 9) {
					Loadwarden(p);
					seleceted.put(p, "Warden");
				}
				if (e.getSlot() == 10) {
					Loadalchemist(p);
					seleceted.put(p, "Alchemist");
				}
				if (e.getSlot() == 11) {
					Loadgoat(p);
					seleceted.put(p, "Goat");
				}
				if (e.getSlot() == 12) {
					Loadshark(p);
					seleceted.put(p, "Shark");
				}
				if (e.getSlot() == 13) {
					Loadassassin(p);
					seleceted.put(p, "Assassin");
				}
				if (e.getSlot() == 14) {
					Loadflagellant(p);
					seleceted.put(p, "Flagellant");
				}
				if (e.getSlot() == 15) {
					Loadace(p);
					seleceted.put(p, "Ace");
				}
				if (e.getSlot() == 16) {
					Loadpaladin(p);
					seleceted.put(p, "Paladin");
				}
				if (e.getSlot() == 17) {
					Loadconfuser(p);
					seleceted.put(p, "Confuser");
				}
				e.setCancelled(true);
			}
		}
	

	private void LoadWarrior(Player player) {
		
		player.getInventory().clear();			
		Location back = new Location(player.getWorld(), -261, 71, 161);
		
		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
		
		//item here
		ItemStack body = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			ItemMeta be = body.getItemMeta();
			be.setUnbreakable(true);
			body.setItemMeta(be);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(255, 255, 255));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(255, 255, 255));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
		ItemStack hand = new ItemStack(Material.STONE_SWORD);
			ItemMeta e = leg.getItemMeta();
			e.setUnbreakable(true);
			hand.setItemMeta(e);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		ItemStack eat2 = new ItemStack(Material.GOLDEN_APPLE, 1);
		//give item
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat, eat2);
	}
	

	private void LoadArcher(Player player) {
		
		player.getInventory().clear();			
					
		Location fix = new Location(player.getWorld(), -336, 97, 180);
		
		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
		//item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(50, 205, 50));
			he.setUnbreakable(true);
			head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(50, 205, 50));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(50, 205, 50));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.IRON_BOOTS);
		ItemStack hand = new ItemStack(Material.BOW);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
		ItemStack arrow = new ItemStack(Material.ARROW, 32);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		ItemStack melee = new ItemStack(Material.SPRUCE_SAPLING);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.BLUE + "SUPERB SLAPING");
			melee.setItemMeta(meleemeta);
		melee.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
		//give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, melee, eat, arrow);
		{
}
}
	private void LoadBlaze(Player player) {
		player.getInventory().clear();			
		
		Location fix = new Location(player.getWorld(), -246, 77, 143);
		
		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
		//item here
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(142, 101, 39));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(251, 172, 45));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
			foo.setColor(Color.fromRGB(251, 172, 45));
			foo.setUnbreakable(true);
			foot.setItemMeta(foo);
		ItemStack hand = new ItemStack(Material.BOW);
			hand.addEnchantment(Enchantment.ARROW_FIRE, 1);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
		ItemStack arrow = new ItemStack(Material.ARROW, 15);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		ItemStack melee = new ItemStack(Material.BLAZE_ROD);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.RED + "HOT ROD");
			melee.setItemMeta(meleemeta);
		ItemStack floa = new ItemStack(Material.FEATHER);
			ItemMeta meleemet = floa.getItemMeta();
			meleemet.setDisplayName(ChatColor.WHITE + "Float");
			meleemet.setLore(Arrays.asList("Right click to Fly","Cooldown: 30 sec"));
			floa.setItemMeta(meleemet);

		
			
		melee.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
		//give item
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, melee, eat, arrow, floa);
	}
		

	private void LoadBulwark(Player player) {
		
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -374, 72, 219);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

		// item here
		ItemStack head = new ItemStack(Material.IRON_HELMET);
		head.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemMeta he = head.getItemMeta();
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.IRON_CHESTPLATE);
		body.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemMeta h = body.getItemMeta();
		h.setUnbreakable(true);
		body.setItemMeta(h);
		ItemStack leg = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta bod = leg.getItemMeta();
		bod.setUnbreakable(true);
		leg.setItemMeta(bod);
		ItemStack foot = new ItemStack(Material.IRON_BOOTS);
		ItemMeta q = foot.getItemMeta();
		q.setUnbreakable(true);
		foot.setItemMeta(q);
		ItemStack hand = new ItemStack(Material.SHIELD);
		hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
		hand.addUnsafeEnchantment(Enchantment.THORNS, 1);
		ItemMeta meleemeta = hand.getItemMeta();
		meleemeta.setUnbreakable(true);
		meleemeta.setDisplayName(ChatColor.GRAY + "Spiked shield");
		hand.setItemMeta(meleemeta);

		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat);
	{}

}
	private void Loadcactus(Player player) {
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -253, 73, 196);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

		// item here
		ItemStack head = new ItemStack(Material.CACTUS);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		body.addUnsafeEnchantment(Enchantment.THORNS, 14);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.GREEN);
		bod.setUnbreakable(true);
		body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		leg.addUnsafeEnchantment(Enchantment.THORNS, 14);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.GREEN);
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		foot.addUnsafeEnchantment(Enchantment.THORNS, 14);
		LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
		foo.setColor(Color.GREEN);
		foo.setUnbreakable(true);
		foot.setItemMeta(foo);
		ItemStack hand = new ItemStack(Material.STONE_SWORD);
		ItemMeta he = hand.getItemMeta();
		he.setUnbreakable(true);
		hand.setItemMeta(he);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat);
	}

	private void Loadcat(Player player) {
		player.getInventory().clear();			
		Location back = new Location(player.getWorld(), -290, 74, 214);
		
		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
		
		//item here

		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(255, 165, 0));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(255, 165, 0));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(255, 165, 0));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
		
		foot.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 6);
		ItemStack melee = new ItemStack(Material.PUMPKIN_SEEDS);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.YELLOW + "Cat's claw");
			melee.setItemMeta(meleemeta);
		melee.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
			ItemMeta he = melee.getItemMeta();
			he.setUnbreakable(true);
			melee.setItemMeta(he);
		ItemStack eat = new ItemStack(Material.COOKED_COD, 16);
		//give item

		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(melee, eat);
	}
	
	private void LoadExecutioner(Player player) {
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -354, 74, 194);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

		player.teleport(back);

		// item here
		ItemStack head = new ItemStack(Material.BLACK_WOOL);
		ItemStack leg = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta le = leg.getItemMeta();
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
		fo.setUnbreakable(true);
		foot.setItemMeta(fo);
		ItemStack hand = new ItemStack(Material.IRON_AXE);
		ItemMeta l = hand.getItemMeta();
		l.setUnbreakable(true);
		hand.setItemMeta(l);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat);
	}
	private void Loadimpaler(Player player) {
		
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -277, 72, 225);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
		he.setColor(Color.fromRGB(0, 255, 255));
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.fromRGB(0, 255, 255));
		bod.setUnbreakable(true);
		body.setItemMeta(bod);
		AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", 5.5, Operation.ADD_NUMBER);
		AttributeModifier speed = new AttributeModifier(UUID.randomUUID(), "generic.attackSpeed", -2.9, Operation.ADD_NUMBER);
		ItemStack hand = new ItemStack(Material.TRIDENT);
		hand.addEnchantment(Enchantment.LOYALTY, 1);
		hand.addEnchantment(Enchantment.VANISHING_CURSE, 1);
		ItemMeta le = hand.getItemMeta();
		le.setUnbreakable(true);
		le.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
		le.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, speed);
		hand.setItemMeta(le);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);

		player.getInventory().addItem(hand, eat);
	}
	private void Loadstalker(Player player) {
		
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -340, 73, 167);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here

		ItemStack hand = new ItemStack(Material.IRON_SWORD);
		ItemStack cloak = new ItemStack(Material.SUGAR);
		ItemMeta cl = cloak.getItemMeta();
		cl.setDisplayName(ChatColor.AQUA + "Cloak");
		cl.setLore(Arrays.asList("Right click to change mode to cloak"));
		cloak.setItemMeta(cl);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().addItem(hand,cloak ,eat);
	}
	private void Loadwarden(Player player) {
		
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -266, 64, 14);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999, 2));
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.CYAN_WOOL, 1);
		ItemStack body = new ItemStack(Material.NETHERITE_CHESTPLATE, 1);
		ItemMeta le = body.getItemMeta();
		le.setUnbreakable(true);
		body.setItemMeta(le);
		ItemStack skill = new ItemStack(Material.SOUL_CAMPFIRE);
		ItemMeta ability = skill.getItemMeta();
		ability.setDisplayName(ChatColor.AQUA + "Wrath of the fallen");
		ability.setLore(Arrays.asList("Right click to locate nearby player and release your wrath","Cooldown: 40 sec"));
		skill.setItemMeta(ability);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);

		player.getInventory().addItem(eat,skill);
	}
	private void Loadalchemist(Player player) {
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -312, 73, 216);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
		he.setColor(Color.fromRGB(139, 0, 139));
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
		ItemMeta h = hand.getItemMeta();
		h.setUnbreakable(true);
		hand.setItemMeta(h);

		ItemStack p1 = new ItemStack(Material.SPLASH_POTION, 8);
		PotionMeta pm = (PotionMeta) p1.getItemMeta();
		pm.setMainEffect(PotionEffectType.HARM);
		PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 20, 0);
		pm.addCustomEffect(harm, true);
		pm.setDisplayName("�4HARMING POTION");
		p1.setItemMeta(pm);
		ItemStack p2 = new ItemStack(Material.SPLASH_POTION, 4);
		PotionMeta pm2 = (PotionMeta) p2.getItemMeta();
		pm2.setMainEffect(PotionEffectType.POISON);
		PotionEffect poi = new PotionEffect(PotionEffectType.POISON, 200, 2);
		pm2.addCustomEffect(poi, true);
		pm2.setDisplayName("�2POISION POTION");
		p2.setItemMeta(pm2);
		ItemStack p3 = new ItemStack(Material.SPLASH_POTION, 3);
		PotionMeta pm3 = (PotionMeta) p3.getItemMeta();
		pm3.setMainEffect(PotionEffectType.WEAKNESS);
		PotionEffect weak = new PotionEffect(PotionEffectType.WEAKNESS, 150, 2);
		pm3.addCustomEffect(weak, true);
		pm3.setDisplayName("�dWEAKNESS POTION");
		p3.setItemMeta(pm3);
		ItemStack p4 = new ItemStack(Material.SPLASH_POTION, 4);
		PotionMeta pm4 = (PotionMeta) p4.getItemMeta();
		pm4.setMainEffect(PotionEffectType.SLOW);
		PotionEffect slow = new PotionEffect(PotionEffectType.SLOW, 275, 3);
		pm4.addCustomEffect(slow, true);
		pm4.setDisplayName("�9SLOWNESS POTION");
		p4.setItemMeta(pm4);
		ItemStack p5 = new ItemStack(Material.POTION, 3);
		PotionMeta pm5 = (PotionMeta) p5.getItemMeta();
		pm5.setMainEffect(PotionEffectType.HEAL);
		PotionEffect ins_h = new PotionEffect(PotionEffectType.HEAL, 1, 0);
		pm5.addCustomEffect(ins_h, true);
		pm5.setDisplayName("�cINSTA HEAL POTION");
		p5.setItemMeta(pm5);
		ItemStack p6 = new ItemStack(Material.POTION, 3);
		PotionMeta pm6 = (PotionMeta) p6.getItemMeta();
		pm6.setMainEffect(PotionEffectType.SPEED);
		PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, 160, 2);
		pm6.addCustomEffect(speed, true);
		pm6.setDisplayName("�bSPEED POTION");
		p6.setItemMeta(pm6);
		ItemStack p7 = new ItemStack(Material.LINGERING_POTION, 8);
		PotionMeta pm7 = (PotionMeta) p7.getItemMeta();
		pm7.setMainEffect(PotionEffectType.POISON);
		PotionEffect glow = new PotionEffect(PotionEffectType.GLOWING, 450, 2);
		pm7.addCustomEffect(glow, true);
		pm7.setDisplayName("�fANTI-STALKER POTION");
		p7.setItemMeta(pm7);
		ItemStack p8 = new ItemStack(Material.POTION, 1);
		PotionMeta pm8 = (PotionMeta) p8.getItemMeta();
		pm8.setMainEffect(PotionEffectType.INCREASE_DAMAGE);
		PotionEffect str = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 2, 1);
		pm8.setMainEffect(PotionEffectType.SPEED);
		pm8.setMainEffect(PotionEffectType.INCREASE_DAMAGE);
		
		pm8.addCustomEffect(str, true);
		pm8.setDisplayName("�0UBER TOXIN");
		p8.setItemMeta(pm8);

		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().addItem(hand, eat, p1, p2, p3, p4, p5, p6, p7);
	}
	private void Loadgoat(Player player) {
		
		player.getInventory().clear();

		Location fix = new Location(player.getWorld(), -313, 88, 146);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

		player.teleport(fix);
		// item here
		ItemStack head = new ItemStack(Material.GRAY_WOOL);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.fromRGB(255, 255, 255));
		bod.setUnbreakable(true);
		body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(255, 255, 255));
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
		foo.setColor(Color.fromRGB(255, 255, 255));
		foo.setUnbreakable(true);
		foot.setItemMeta(foo);
		ItemStack hand = new ItemStack(Material.DIORITE);
		hand.addUnsafeEnchantment(Enchantment.KNOCKBACK, 3);
		hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
		ItemMeta meleemeta = hand.getItemMeta();
		meleemeta.setDisplayName(ChatColor.WHITE + "RAM!");
		hand.setItemMeta(meleemeta);
		ItemStack hand2 = new ItemStack(Material.WOODEN_SWORD);
		hand2.addUnsafeEnchantment(Enchantment.KNOCKBACK, 12);
		ItemMeta meleemeta2 = hand2.getItemMeta();
		meleemeta2.setDisplayName(ChatColor.GREEN + "GOOGLE CHROME");
		hand2.setItemMeta(meleemeta2);
		hand2.setDurability((short) 58);
		ItemStack eat = new ItemStack(Material.BREAD, 16);

		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, hand2, eat);
	}
	private void Loadshark(Player player) {
		
		player.getInventory().clear();			
		Location back = new Location(player.getWorld(), -322, 69, 252);
		
		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
		player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING,Integer.MAX_VALUE,100));
		//item here
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(0, 0, 93));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(0, 0, 93));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.IRON_BOOTS);
			ItemMeta l = foot.getItemMeta();
			l.setUnbreakable(true);
			foot.setItemMeta(l);
			foot.addEnchantment(Enchantment.DEPTH_STRIDER, 3);
		ItemStack hand = new ItemStack(Material.IRON_NUGGET);
		 ItemMeta handM = hand.getItemMeta();
		 handM.setDisplayName(ChatColor.AQUA + "Shark's Tooth");
		 handM.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		 hand.setItemMeta(handM);
		ItemStack eat = new ItemStack(Material.COOKED_SALMON, 20);
		//give item
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat);
	}
	private void Loadassassin(Player player) {
		
		player.getInventory().clear();

		Location fix = new Location(player.getWorld(), -345, 74, 242);

		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
		he.setColor(Color.fromRGB(0, 0, 0));
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.fromRGB(0, 0, 0));
		bod.setUnbreakable(true);
		body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(0, 0, 0));
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
		fo.setColor(Color.fromRGB(0, 0, 0));
		fo.setUnbreakable(true);
		foot.setItemMeta(fo);
		ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
		ItemMeta h = hand.getItemMeta();
		h.setUnbreakable(true);
		hand.setItemMeta(h);
		AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", 10, Operation.ADD_NUMBER);
		AttributeModifier speed = new AttributeModifier(UUID.randomUUID(), "generic.attackSpeed", -3, Operation.ADD_NUMBER);
		ItemStack Ani = new ItemStack(Material.GOLDEN_SWORD);
		ItemMeta meleemeta = Ani.getItemMeta();
		meleemeta.setDisplayName(ChatColor.BLUE + "Orphan Obliterator");
		meleemeta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
		meleemeta.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, speed);
		Ani.setItemMeta(meleemeta);
		Ani.setDurability((short) 31);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);

		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, Ani, eat);
	}
	private void Loadflagellant(Player player) {
		
		player.getInventory().clear();

		Location fix = new Location(player.getWorld(), -303, 73, 247);

		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
		he.setColor(Color.fromRGB(110, 0, 0));
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.fromRGB(100, 0, 0));
		bod.setUnbreakable(true);
		body.setItemMeta(bod);

		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(110, 0, 0));
		le.setUnbreakable(true);
		leg.setItemMeta(le);

		ItemStack foot = new ItemStack(Material.CHAINMAIL_BOOTS);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 12);
		ItemStack melee = new ItemStack(Material.LEAD);
		ItemMeta meleemeta = melee.getItemMeta();
		meleemeta.setDisplayName(ChatColor.RED + "Blood sloaked whip");
		meleemeta.setLore(Arrays.asList("Right click to sacrifice your health and gain strength"));
		melee.setItemMeta(meleemeta);
		melee.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
		ItemStack pain = new ItemStack(Material.RED_DYE, 3);
		ItemMeta meleemet = pain.getItemMeta();
		meleemet.setDisplayName(ChatColor.RED + "PAIN FOR GAIN!");
		meleemeta.setLore(Arrays.asList("Right click to sacrifice your health and gain strength and speed"));
		pain.setItemMeta(meleemet);
		ItemStack rest = new ItemStack(Material.GREEN_DYE, 3);
		ItemMeta meleeme = rest.getItemMeta();
		meleeme.setDisplayName(ChatColor.GREEN + "*Rest the mind*");
		meleeme.setLore(Arrays.asList("Right click to get into rest mode"));
		rest.setItemMeta(meleeme);
		ItemStack re = new ItemStack(Material.GRAY_DYE, 1);
		ItemMeta meleem = re.getItemMeta();
		meleem.setDisplayName("REPLENISH");
		meleem.setLore(Arrays.asList("Right click to sacrifice your hunger for skill"));
		re.setItemMeta(meleem);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(melee, eat, pain, rest, re);
	}
	private void Loadace(Player player) {
		
		player.getInventory().clear();
		Location back = new Location(player.getWorld(), -303, 201, 194);

		player.teleport(back);
		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 60, 2));

		// item here
		ItemStack head = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
		he.setColor(Color.fromRGB(0, 0, 0));
		he.setUnbreakable(true);
		head.setItemMeta(he);
		ItemStack body = new ItemStack(Material.ELYTRA);
		ItemMeta be = body.getItemMeta();
		be.setUnbreakable(true);
		body.setItemMeta(be);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(41, 51, 0));
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
		fo.setColor(Color.fromRGB(41, 51, 0));
		fo.setUnbreakable(true);
		foot.setItemMeta(fo);
		ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
		ItemMeta e = leg.getItemMeta();
		e.setUnbreakable(true);
		hand.setItemMeta(e);
		ItemStack p1 = new ItemStack(Material.SPLASH_POTION, 15);
		PotionMeta pm = (PotionMeta) p1.getItemMeta();
		pm.setMainEffect(PotionEffectType.HARM);
		PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 20, 1);
		pm.addCustomEffect(harm, true);
		pm.setDisplayName("�4Carpet Bomb");
		p1.setItemMeta(pm);
		ItemStack p2 = new ItemStack(Material.LINGERING_POTION, 5);
		PotionMeta pm2 = (PotionMeta) p2.getItemMeta();
		pm2.setMainEffect(PotionEffectType.HARM);
		PotionEffect poi = new PotionEffect(PotionEffectType.HARM, 200, 2);
		pm2.addCustomEffect(poi, true);
		pm2.setDisplayName("�cMOLOTOV");
		p2.setItemMeta(pm2);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat, p1, p2);
	}
	private void Loadpaladin(Player player) {

		player.getInventory().clear();

		Location fix = new Location(player.getWorld(), -388, 72, 199);

		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.IRON_HELMET);
		ItemStack body = new ItemStack(Material.IRON_CHESTPLATE);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(242, 242, 242));
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
		fo.setColor(Color.fromRGB(242, 242, 242));
		fo.setUnbreakable(true);
		foot.setItemMeta(fo);
		ItemStack hand = new ItemStack(Material.IRON_SHOVEL);
		ItemMeta h = hand.getItemMeta();
		h.setUnbreakable(true);
		h.setDisplayName(ChatColor.GRAY + "Iron mace");
		hand.setItemMeta(h);
		hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		ItemStack Flag1 = new ItemStack(Material.LIME_BANNER);
		ItemMeta f1 = Flag1.getItemMeta();
		f1.setDisplayName(ChatColor.GREEN + "Banner of Life");
		f1.setLore(Arrays.asList("Right click to give regeneration to you and your ally"));
		Flag1.setItemMeta(f1);
		ItemStack Flag2 = new ItemStack(Material.RED_BANNER);
		ItemMeta f2 = Flag2.getItemMeta();
		f2.setDisplayName(ChatColor.RED + "Banner of War");
		f2.setLore(Arrays.asList("Right click to damage everyone in radius"));
		Flag2.setItemMeta(f2);
		ItemStack Flag3 = new ItemStack(Material.YELLOW_BANNER);
		ItemMeta f3 = Flag3.getItemMeta();
		f3.setDisplayName(ChatColor.YELLOW + "Banner of Haste");
		f3.setLore(Arrays.asList("Right click to give speed to you and your ally"));
		Flag3.setItemMeta(f3);
		ItemStack Flag4 = new ItemStack(Material.BLUE_BANNER);
		ItemMeta f4 = Flag4.getItemMeta();
		f4.setDisplayName(ChatColor.AQUA + "Banner of Power");
		f4.setLore(Arrays.asList("Right click to give protection to you and your ally"));
		Flag4.setItemMeta(f4);
		ItemStack Flag5 = new ItemStack(Material.WHITE_BANNER);
		ItemMeta f5 = Flag5.getItemMeta();
		f5.setDisplayName(ChatColor.WHITE + "Banner of Seer");
		f5.setLore(Arrays.asList("Right click to reveal nearby enemy"));
		Flag5.setItemMeta(f5);

		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat, Flag1, Flag2, Flag3, Flag4, Flag5);
	}
	private void Loadconfuser(Player player) {
		
		player.getInventory().clear();

		Location fix = new Location(player.getWorld(), -337, 76, 256);

		player.teleport(fix);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
		// item here
		ItemStack head = new ItemStack(Material.RED_MUSHROOM_BLOCK);
		ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
		bod.setColor(Color.fromRGB(204, 0, 153));
		bod.setUnbreakable(true);
		body.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		body.setItemMeta(bod);
		ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
		le.setColor(Color.fromRGB(255, 120, 0));
		le.setUnbreakable(true);
		leg.setItemMeta(le);
		ItemStack foot = new ItemStack(Material.IRON_BOOTS);
		ItemStack hand = new ItemStack(Material.STONE_SWORD);
		ItemMeta h = hand.getItemMeta();
		h.setUnbreakable(true);
		hand.setItemMeta(h);
		ItemStack sus = new ItemStack(Material.RED_MUSHROOM);
		ItemMeta su = sus.getItemMeta();
		su.setDisplayName("Confusing Shroom");
		su.setLore(Arrays.asList("Right click to give random effect to everyone in radius"));
		sus.setItemMeta(su);
		ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
		// give item
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(body);
		player.getInventory().setLeggings(leg);
		player.getInventory().setBoots(foot);
		player.getInventory().addItem(hand, eat, sus);
	}
}
		
	

