package me.tanaphat.inv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Trident;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_16_R3.EntityProjectileThrowable;
import net.minecraft.server.v1_16_R3.EntityThrownTrident;

public class Testsomething implements Listener {
	HashMap<String, String> tri = new HashMap<String, String>();
	private Main plugin;
	
	public Testsomething(Main craft) {
		plugin = craft;
	}
	
	@EventHandler
	public void change(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		p.getGameMode();
	}
	//	p.setHealth(p.getHealth() + 5); HEALLLLLLL
	@EventHandler
	public void PlayTheSound(PlayerJoinEvent j) {
		Player p = j.getPlayer();
		Menu.seleceted.put(p, "Join");
		p.sendMessage(ChatColor.LIGHT_PURPLE + "Welcome to the prototype ClassPvP server, Have fun!");
		p.sendMessage(ChatColor.BLUE + "DISCORD: https://discord.gg/7fQR8tH4Tg");
		p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, (float) 1, (float) 1);
		Location back = new Location(p.getWorld(), -215, 112, 167);
		p.damage(999);
		p.teleport(back);
		p.getInventory().clear();
	
		
		p.discoverRecipe(new NamespacedKey(plugin, "bottle"));
		p.discoverRecipe(new NamespacedKey(plugin, "p2"));
		p.discoverRecipe(new NamespacedKey(plugin, "p3"));
		p.discoverRecipe(new NamespacedKey(plugin, "p4"));
		p.discoverRecipe(new NamespacedKey(plugin, "p5"));
		p.discoverRecipe(new NamespacedKey(plugin, "p6"));
		p.discoverRecipe(new NamespacedKey(plugin, "ophran"));
	}

	// เดี๋ยวทำต่อ

	
	@EventHandler
	public void kill(PlayerDeathEvent event) {
		Player p = event.getEntity();
		Player killer = p.getKiller();
		if (killer instanceof Player) {
			for (int i = 0; i < wolf_list.size(); i++) {
				String[] explode = wolf_list.get(i).split(":");

				for (World wold : Bukkit.getWorlds()) {
					for (Entity entity : wold.getEntities()) {
						if (entity.getType() == EntityType.WOLF) {
							Wolf wolf = (Wolf) entity;
							if (p.getName().equals(explode[0])) {
								if (wolf.getUniqueId().equals(UUID.fromString(explode[1]))) {
									wolf.remove();
								}
								wolf_list.remove(explode[0] + ":" + explode[1]);
							}
						}
					}
				}
			}
			System.out.println(wolf_list);
		}
	}
	
	@EventHandler
	public void reset(PlayerDeathEvent d) {
		Player p = d.getEntity();
		Player killer = p.getKiller();
		if (killer instanceof Player) {
			Menu.seleceted.put(p, "Join");
			p.getInventory().clear();

		} else {
			Menu.seleceted.put(p, "Join");
			p.getInventory().clear();
		}
	}

	@EventHandler
	public void joinarray(PlayerJoinEvent j) {
		Player p = j.getPlayer();
		Menu.seleceted.remove(p);
	}

	@EventHandler
	public void Mapwarp(PlayerInteractEvent event) {
		Action action = event.getAction();
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();

		if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (block.getType().equals(Material.END_PORTAL_FRAME)) {
				Location back = new Location(player.getWorld(), -316, 73, 198);

				player.teleport(back);
				player.playSound(player.getLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, (float) 1, (float) 1);
			} else {
			}
		}
	}

	ArrayList<String> wolf_list = new ArrayList<String>();

	@EventHandler
	public void onPlayerClicks(PlayerInteractEvent event) {

		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.WOLF_SPAWN_EGG) {

				player.getInventory().getItemInMainHand()
						.setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);

				Wolf wolf = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				wolf.setAdult();
				wolf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999, 1));
				wolf.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 400, 1));
				wolf.setOwner(player);
				wolf_list.add(player.getName() + ":" + wolf.getUniqueId());

				System.out.println(wolf_list);
			}
		}
	}

	@EventHandler
	public void ShroomDrunk(PlayerInteractEvent event) {
		Action action = event.getAction();
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();

		if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (block.getType().equals(Material.MUSHROOM_STEM)) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 140, 3));

				player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_BURP, (float) 1, (float) 1);
			} else {
			}
		}
	}

	@EventHandler
	public void onswapitem(PlayerSwapHandItemsEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void ondropitem(PlayerDropItemEvent e) {
		e.setCancelled(true);
	}

	

}