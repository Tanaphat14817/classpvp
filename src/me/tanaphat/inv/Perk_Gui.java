package me.tanaphat.inv;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Perk_Gui implements CommandExecutor{
@Override
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			
			Inventory gui = Bukkit.createInventory(p, 9,"Perk");
			ItemStack item1 = new ItemStack(Material.GOLD_INGOT, 1);
			ItemMeta item1_meta = item1.getItemMeta();
			item1_meta.setDisplayName("Golden Head");
			item1_meta.setLore(Arrays.asList("Gain Golden head instead of Golden apple"));
			item1.setItemMeta(item1_meta);
			
			ItemStack item2 = new ItemStack(Material.SHIELD, 1);
			ItemMeta item2_meta = item2.getItemMeta();
			item2_meta.setDisplayName("Kill Streaker");
			item2_meta.setLore(Arrays.asList("Gain resistance after a kill"));
			item2.setItemMeta(item2_meta);
			
			ItemStack item3 = new ItemStack(Material.BARRIER, 1);
			ItemMeta item3_meta = item3.getItemMeta();
			item3_meta.setDisplayName("No perk");
			item3_meta.setLore(Arrays.asList("title says it all"));
			item3.setItemMeta(item3_meta);
			
			gui.addItem(item1);
			gui.addItem(item2);
			gui.addItem(item3);
			
			p.openInventory(gui);
}
		return true;
}
}
