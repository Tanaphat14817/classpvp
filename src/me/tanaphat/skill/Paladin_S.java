package me.tanaphat.skill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Paladin_S implements Listener {

	@EventHandler
	public void PaladinRed(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack is = p.getInventory().getItemInMainHand();
			if (!is.hasItemMeta()) {
				return;
			}
			ItemMeta im = is.getItemMeta();
			String displayname = ChatColor.stripColor(im.getDisplayName()); //here!!!
			if (displayname.equals("Banner of War")) {
				p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH,  1f,  1f);
				p.getActivePotionEffects().clear();
				p.damage(3);
				p.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 80, 1));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(p.getLocation()) <= 4) {
						p.getActivePotionEffects().clear();
						other.damage(4);
					}
				}
			} else if (displayname.equals("Banner of Life")) {
				p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
				p.getActivePotionEffects().clear();
				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 40, 1));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 3));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(p.getLocation()) <= 3) {
						p.getActivePotionEffects().clear();
						other.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 60, 1));
					}
				}
			} else if (displayname.equals("Banner of Haste")) {
				p.getActivePotionEffects().clear();
				p.playSound(p.getLocation(), Sound.BLOCK_DISPENSER_LAUNCH, 1f, 1f);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 0));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(p.getLocation()) <= 4) {
						other.getActivePotionEffects().clear();
						other.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 0));
					}
				}
			} else if (displayname.equals("Banner of Power")) {
				p.getActivePotionEffects().clear();
				p.playSound(p.getLocation(), Sound.ITEM_ARMOR_EQUIP_CHAIN, 1f, 1f);
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(p.getLocation()) <= 3) {
						other.getActivePotionEffects().clear();
						other.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
					}
				}
			} else if (displayname.equals("Banner of Seer")) {
				p.getActivePotionEffects().clear();
				p.playSound(p.getLocation(), Sound.ENTITY_ENDER_EYE_DEATH, 1f, 1f);
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 1));
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 80, 2));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 4));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(p.getLocation()) <= 4) {
						other.getActivePotionEffects().clear();
						other.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 80, 1));
					}
				}
			}

		}
	}
}