package me.tanaphat.skill;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.tanaphat.inv.Menu;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Confuser_S implements Listener{
	
	HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	Random rand = new Random();
	@EventHandler
	public void Flagellant(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!is.hasItemMeta()) {
				return;
			}
			ItemMeta im = is.getItemMeta();
			String displayname = ChatColor.stripColor(im.getDisplayName()); 
			if (displayname.equals("Confusing Shroom")) {
				
				if (cooldowns.containsKey(player.getName())) {
					if(cooldowns.get(player.getName()) > System.currentTimeMillis()) {
						long timeleft = (cooldowns.get(player.getName()) - System.currentTimeMillis()) / 1000;
						player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.WHITE + "Ability will be ready in " + ChatColor.YELLOW + timeleft + " second(s)"));
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, (float) 1, (float) 1);
						return;
					}
				}
				
				cooldowns.put(player.getName() , System.currentTimeMillis() + (10 * 1000));
				
				for (Player other : Bukkit.getOnlinePlayers()) {
					if(other.getLocation().distance(player.getLocation()) <=12 ) {
						
						player.playSound(player.getLocation(), Sound.ENTITY_SPLASH_POTION_BREAK, (float) 1, (float) 1);
						int chance = rand.nextInt(26);
						 
						 if(chance == 0) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 200, 2));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Absorption from confuser!");
						 }
						 if(chance == 1) {	
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Blindness from confuser!");
							 player.removePotionEffect(PotionEffectType.BLINDNESS);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 2) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 2));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Nausea from confuser!");
							 player.removePotionEffect(PotionEffectType.CONFUSION);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 3) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Resistance from confuser!");
						 }
						 if(chance == 4) {
						 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.DOLPHINS_GRACE, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Dolpins grace from confuser!");
						 }
						 if(chance == 5) {
							
							 other.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Haste from confuser!");
						 }
						 if (chance == 6) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Fire resistance from confuser!");
						 }
						 if(chance == 7) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Glowing from confuser!");
						 }
						 if(chance == 8) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.HARM, 40, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Instant damage from confuser!");
							 player.removePotionEffect(PotionEffectType.HARM);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 9) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 40, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Instant health from confuser!");
						 }
						 if(chance == 10) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 200, 2));
							 other.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 40, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Health boost  from confuser!");
						 }
						 if(chance == 11) {
						 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 200, 4));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Hunger from confuser!");
							 player.removePotionEffect(PotionEffectType.HUNGER);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 12) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Strength from confuser!");
						 }
						 if(chance == 13) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Invisibility from confuser!");
						 }
						 if(chance == 14) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Jump boost from confuser!");
						 }
						 if(chance == 15) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Levitation from confuser!");
						 }
						 if(chance == 16) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 200, 0));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Night vision from confuser!");
						 }
						 if(chance == 17) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Poision from confuser!");
							 player.removePotionEffect(PotionEffectType.POISON);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 19) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Regeneration from confuser!");
						 }
						 if(chance == 20) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 200, 2));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Saturation from confuser!");
						 }
						 if(chance == 21) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Slowness from confuser!");
							 player.removePotionEffect(PotionEffectType.SLOW);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 22) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 2));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Mining fatigue from confuser!");
							 player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 23) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Slow falling from confuser!");
						 }
						 if(chance == 24) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Speed from confuser!");
						 }
						 if(chance == 25) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Water breathing from confuser!");
						 }
						 if(chance == 26) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Weakness from confuser!");
							 player.removePotionEffect(PotionEffectType.WEAKNESS);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
						 if(chance == 27) {
							 
							 other.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 200, 1));
							 other.sendMessage(ChatColor.LIGHT_PURPLE + "you got Wither from confuser!");
							 player.removePotionEffect(PotionEffectType.WITHER);
							 player.sendMessage(ChatColor.LIGHT_PURPLE + "But you resist!");
						 }
							 
						 
						
						
					}
					

	
	}
}
}
	}
}
	
