package me.tanaphat.skill;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import me.tanaphat.inv.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Blaze_S implements Listener {

	HashMap<String, Long> cooldowns = new HashMap<String, Long>();


	@EventHandler
	public void Blaze_skill(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.FEATHER) {
					
					if (cooldowns.containsKey(player.getName())) {
						if(cooldowns.get(player.getName()) > System.currentTimeMillis()) {
							long timeleft = (cooldowns.get(player.getName()) - System.currentTimeMillis()) / 1000;
							player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.WHITE + "Ability will be ready in " + ChatColor.YELLOW + timeleft + " second(s)"));
							player.playSound(player.getLocation(), Sound.BLOCK_DISPENSER_FAIL, (float) 1, (float) 1);
							return;
						}
					}
					
					cooldowns.put(player.getName() , System.currentTimeMillis() + (30 * 1000));
					
					player.playSound(player.getLocation(), Sound.ENTITY_PARROT_FLY, (float) 1, (float) 1);
					player.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 160, 1));
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 240, 1));
				}
			}
		}
	}


