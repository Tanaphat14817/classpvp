package me.tanaphat.skill;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import me.tanaphat.inv.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Warden_S implements Listener {

	HashMap<String, Long> cooldowns = new HashMap<String, Long>();

	@EventHandler
	public void Blaze_skill(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!is.hasItemMeta()) {
				return;
			}
			ItemMeta im = is.getItemMeta();
			String displayname = im.getDisplayName().replaceAll("�c", "").replaceAll("�a", "").replaceAll("�e", "").replaceAll("�f", "").replaceAll("�b", "");
			if (displayname.equals("Wrath of the fallen")) {
				if (cooldowns.containsKey(player.getName())) {
					if (cooldowns.get(player.getName()) > System.currentTimeMillis()) {
						long timeleft = (cooldowns.get(player.getName()) - System.currentTimeMillis()) / 1000;
						player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.WHITE
								+ "Ability will be ready in " + ChatColor.YELLOW + timeleft + " second(s)"));
						player.playSound(player.getLocation(), Sound.BLOCK_DISPENSER_FAIL, (float) 1, (float) 1);
						return;
					}
				}

				cooldowns.put(player.getName(), System.currentTimeMillis() + (50 * 1000));
				
				player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, (float) 1, (float) 1);
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 400, 4));
				for (Player other : Bukkit.getOnlinePlayers()) {
					if (other.getLocation().distance(player.getLocation()) <= 25) {
						player.getActivePotionEffects().clear();
						other.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 400, 1));
						player.removePotionEffect(PotionEffectType.GLOWING);
					}
				}
			}
		}
	}
}
