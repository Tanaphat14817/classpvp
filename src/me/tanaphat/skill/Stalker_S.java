package me.tanaphat.skill;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Stalker_S implements Listener {

	@EventHandler
	public void Stalker_skill1(PlayerInteractEvent event) {

		HashMap<String, Long> cooldowns = new HashMap<String, Long>();

		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!is.hasItemMeta()) {
				return;
			}

				ItemMeta im = is.getItemMeta();
				String displayname = ChatColor.stripColor(im.getDisplayName());
			if (displayname.equals("Cloak")) {

				player.removePotionEffect(PotionEffectType.WEAKNESS);
				player.removePotionEffect(PotionEffectType.SPEED);
				player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 99999, 2));
				player.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 20, 1));
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 99999, 3));
				player.getInventory().getItemInMainHand()
						.setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);

				ItemStack pain = new ItemStack(Material.GUNPOWDER, 1);
				ItemMeta meleemet = pain.getItemMeta();
				meleemet.setDisplayName(ChatColor.RED + "Decloak");
				meleemet.setLore(Arrays.asList("Right click to change mode to decloak"));
				pain.setItemMeta(meleemet);
				player.getInventory().addItem(pain);
			} else if (displayname.equals("Decloak")) {

				player.removePotionEffect(PotionEffectType.INVISIBILITY);
				player.removePotionEffect(PotionEffectType.SLOW);
				player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 999999, 0));
				player.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 40, 1));
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 1));

				player.getInventory().getItemInMainHand()
						.setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);

				ItemStack cloak = new ItemStack(Material.SUGAR);
				ItemMeta cl = cloak.getItemMeta();
				cl.setDisplayName(ChatColor.AQUA + "Cloak");
				cl.setLore(Arrays.asList("Right click to change mode to cloak"));
				cloak.setItemMeta(cl);
				player.getInventory().addItem(cloak);

			}
		}
	}
}
