package me.tanaphat.skill;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Flagallant_S implements Listener {

	@EventHandler
	public void Flagellant(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack is = p.getInventory().getItemInMainHand();
			if (!is.hasItemMeta()) {
				return;
			}
			ItemMeta im = is.getItemMeta();
			String displayname = im.getDisplayName().replaceAll("�c", "").replaceAll("�a", "").replaceAll("�e", "").replaceAll("�f", "").replaceAll("�b", "");
			if (displayname.equals("PAIN FOR GAIN!")) {
				p.damage(8);
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 200, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1));
				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 160, 0));
				p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_SCREAM, (float) 1, (float) 1);
				p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount() - 1);
				p.getInventory().getItemInOffHand().setAmount(p.getInventory().getItemInOffHand().getAmount() - 1);
			} else if (displayname.equals("*Rest the mind*")) {
				p.getActivePotionEffects().clear();
				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 2));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 220, 20));
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 240, 10));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 180, 2));
				p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, (float) 1, (float) 1);
				p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount() - 1);
			} else if (displayname.equals("REPLENISH")) {
				ItemStack pain = new ItemStack(Material.RED_DYE, 1);
				ItemMeta meleemet = pain.getItemMeta();
				meleemet.setDisplayName(ChatColor.RED + "PAIN FOR GAIN!");
				meleemet.setLore(Arrays.asList("Right click to sacrifice your health and gain strength and speed"));
				pain.setItemMeta(meleemet);
				ItemStack rest = new ItemStack(Material.GREEN_DYE, 1);
				ItemMeta meleeme = rest.getItemMeta();
				meleeme.setDisplayName(ChatColor.GREEN + "*Rest the mind*");
				meleeme.setLore(Arrays.asList("Right click to get into rest mode"));
				rest.setItemMeta(meleeme);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 180, 5));
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 100, 2));
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
				p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 100, 90));
				p.getInventory().addItem(pain, rest);
				p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, (float) 1, (float) 1);
				p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount() - 1);
				p.getInventory().getItemInOffHand().setAmount(p.getInventory().getItemInOffHand().getAmount() - 1);
			}
		}
	}

	@EventHandler
	public void Flagellant_skill4(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.LEAD) {
				player.damage(4);
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 35, 1));

			}
		}

	}
}
