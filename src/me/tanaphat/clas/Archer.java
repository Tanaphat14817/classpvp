package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Archer implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 999999, 3));

			Location fix = new Location(player.getWorld(), -336, 97, 180);

			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(50, 205, 50));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(50, 205, 50));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(50, 205, 50));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.IRON_BOOTS);
			ItemStack hand = new ItemStack(Material.BOW);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
			ItemStack arrow = new ItemStack(Material.ARROW, 32);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			ItemStack melee = new ItemStack(Material.SPRUCE_SAPLING);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.BLUE + "SUPERB SLAPING");
			melee.setItemMeta(meleemeta);
			melee.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, melee, eat, arrow);
		}
		return true;
	}

	@EventHandler
	public void onPlayerClicks(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.SPRUCE_SAPLING) {
				player.sendMessage("You have right click a slime ball!");
			}
		}

	}
}