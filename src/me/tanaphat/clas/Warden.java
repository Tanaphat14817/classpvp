package me.tanaphat.clas;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Warden implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -310, 70, 134);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 999999, 3));
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 999999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999, 2));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999, 0));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 999999, 6));
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.CYAN_WOOL, 1);
			ItemStack body = new ItemStack(Material.NETHERITE_CHESTPLATE, 1);
			ItemMeta le = body.getItemMeta();
			le.setUnbreakable(true);
			body.setItemMeta(le);
			ItemStack skill = new ItemStack(Material.SOUL_CAMPFIRE);
			ItemMeta ability = skill.getItemMeta();
			ability.setDisplayName(ChatColor.DARK_BLUE + "Wrath of the fallen");
			ability.setLore(Arrays.asList("Right click to locate nearby player and release your wrath"));
			ability.setLore(Arrays.asList("Cooldown: 40 sec"));
			skill.setItemMeta(ability);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);

			player.getInventory().addItem(eat,skill);
		}
		return true;
	}
}