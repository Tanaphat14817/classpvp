package me.tanaphat.clas;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Alchemist implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -312, 73, 216);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(139, 0, 139));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
			hand.addEnchantment(Enchantment.DAMAGE_ALL, 2);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);

			ItemStack p1 = new ItemStack(Material.SPLASH_POTION, 8);
			PotionMeta pm = (PotionMeta) p1.getItemMeta();
			pm.setMainEffect(PotionEffectType.HARM);
			PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 20, 0);
			pm.addCustomEffect(harm, true);
			pm.setDisplayName("�4HARMING POTION");
			p1.setItemMeta(pm);
			ItemStack p2 = new ItemStack(Material.SPLASH_POTION, 4);
			PotionMeta pm2 = (PotionMeta) p2.getItemMeta();
			pm2.setMainEffect(PotionEffectType.POISON);
			PotionEffect poi = new PotionEffect(PotionEffectType.POISON, 200, 2);
			pm2.addCustomEffect(poi, true);
			pm2.setDisplayName("�2POISION POTION");
			p2.setItemMeta(pm2);
			ItemStack p3 = new ItemStack(Material.SPLASH_POTION, 3);
			PotionMeta pm3 = (PotionMeta) p3.getItemMeta();
			pm3.setMainEffect(PotionEffectType.WEAKNESS);
			PotionEffect weak = new PotionEffect(PotionEffectType.WEAKNESS, 150, 2);
			pm3.addCustomEffect(weak, true);
			pm3.setDisplayName("�dWEAKNESS POTION");
			p3.setItemMeta(pm3);
			ItemStack p4 = new ItemStack(Material.SPLASH_POTION, 4);
			PotionMeta pm4 = (PotionMeta) p4.getItemMeta();
			pm4.setMainEffect(PotionEffectType.SLOW);
			PotionEffect slow = new PotionEffect(PotionEffectType.SLOW, 275, 3);
			pm4.addCustomEffect(slow, true);
			pm4.setDisplayName("�9SLOWNESS POTION");
			p4.setItemMeta(pm4);
			ItemStack p5 = new ItemStack(Material.POTION, 3);
			PotionMeta pm5 = (PotionMeta) p5.getItemMeta();
			pm5.setMainEffect(PotionEffectType.HEAL);
			PotionEffect ins_h = new PotionEffect(PotionEffectType.HEAL, 1, 2);
			pm5.addCustomEffect(ins_h, true);
			pm5.setDisplayName("�cINSTA HEAL POTION");
			p5.setItemMeta(pm5);
			ItemStack p6 = new ItemStack(Material.POTION, 3);
			PotionMeta pm6 = (PotionMeta) p6.getItemMeta();
			pm6.setMainEffect(PotionEffectType.SPEED);
			PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, 160, 2);
			pm6.addCustomEffect(speed, true);
			pm6.setDisplayName("�bSPEED POTION");
			p6.setItemMeta(pm6);
			ItemStack p7 = new ItemStack(Material.LINGERING_POTION, 4);
			PotionMeta pm7 = (PotionMeta) p7.getItemMeta();
			pm7.setMainEffect(PotionEffectType.POISON);
			PotionEffect glow = new PotionEffect(PotionEffectType.GLOWING, 450, 2);
			pm7.addCustomEffect(glow, true);
			pm7.setDisplayName("�fANTI-STALKER POTION");
			p7.setItemMeta(pm7);
			ItemStack p8 = new ItemStack(Material.POTION, 1);
			PotionMeta pm8 = (PotionMeta) p8.getItemMeta();
			pm8.setMainEffect(PotionEffectType.INCREASE_DAMAGE);
			PotionEffect str = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 2, 1);
			pm8.setMainEffect(PotionEffectType.SPEED);
			pm8.setMainEffect(PotionEffectType.INCREASE_DAMAGE);
			
			pm8.addCustomEffect(str, true);
			pm8.setDisplayName("�0UBER TOXIN");
			p8.setItemMeta(pm8);

			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().addItem(hand, eat, p1, p2, p3, p4, p5, p6, p7);
		}
		return true;
	}
}