package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Bulwark implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -374, 72, 219);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			// item here
			ItemStack head = new ItemStack(Material.IRON_HELMET);
			ItemMeta he = head.getItemMeta();
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.IRON_CHESTPLATE);
			ItemMeta h = body.getItemMeta();
			h.setUnbreakable(true);
			body.setItemMeta(h);
			ItemStack leg = new ItemStack(Material.IRON_LEGGINGS);
			ItemMeta bod = leg.getItemMeta();
			bod.setUnbreakable(true);
			leg.setItemMeta(bod);
			ItemStack foot = new ItemStack(Material.IRON_BOOTS);
			ItemMeta q = foot.getItemMeta();
			q.setUnbreakable(true);
			foot.setItemMeta(q);
			ItemStack hand = new ItemStack(Material.SHIELD);
			hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
			hand.addUnsafeEnchantment(Enchantment.THORNS, 1);
			ItemMeta meleemeta = hand.getItemMeta();
			meleemeta.setUnbreakable(true);
			meleemeta.setDisplayName(ChatColor.GRAY + "Spiked shield");
			hand.setItemMeta(meleemeta);

			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat);
		}
		return true;
	}
}
