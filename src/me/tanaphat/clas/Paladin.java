package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Paladin implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();

			Location fix = new Location(player.getWorld(), -388, 72, 199);

			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.IRON_HELMET);
			ItemStack body = new ItemStack(Material.IRON_CHESTPLATE);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(242, 242, 242));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(242, 242, 242));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
			ItemStack hand = new ItemStack(Material.IRON_SHOVEL);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			h.setDisplayName(ChatColor.GRAY + "Iron mace");
			hand.setItemMeta(h);
			hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			ItemStack Flag1 = new ItemStack(Material.LIME_BANNER);
			ItemMeta f1 = Flag1.getItemMeta();
			f1.setDisplayName(ChatColor.GREEN + "Banner of Life");
			Flag1.setItemMeta(f1);
			ItemStack Flag2 = new ItemStack(Material.RED_BANNER);
			ItemMeta f2 = Flag2.getItemMeta();
			f2.setDisplayName(ChatColor.RED + "Banner of War");
			Flag2.setItemMeta(f2);
			ItemStack Flag3 = new ItemStack(Material.YELLOW_BANNER);
			ItemMeta f3 = Flag3.getItemMeta();
			f3.setDisplayName(ChatColor.YELLOW + "Banner of Haste");
			Flag3.setItemMeta(f3);
			ItemStack Flag4 = new ItemStack(Material.BLUE_BANNER);
			ItemMeta f4 = Flag4.getItemMeta();
			f4.setDisplayName(ChatColor.AQUA + "Banner of Power");
			Flag4.setItemMeta(f4);
			ItemStack Flag5 = new ItemStack(Material.WHITE_BANNER);
			ItemMeta f5 = Flag5.getItemMeta();
			f5.setDisplayName(ChatColor.WHITE + "Banner of Seer");
			Flag5.setItemMeta(f5);

			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat, Flag1, Flag2, Flag3, Flag4, Flag5);
		}
		return true;
	}
}