package me.tanaphat.clas;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Warrior implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -261, 71, 161);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			// item here
			ItemStack body = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			ItemMeta be = body.getItemMeta();
			be.setUnbreakable(true);
			body.setItemMeta(be);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(255, 255, 255));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(255, 255, 255));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
			ItemStack hand = new ItemStack(Material.STONE_SWORD);
			ItemMeta e = leg.getItemMeta();
			e.setUnbreakable(true);
			hand.setItemMeta(e);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			ItemStack eat2 = new ItemStack(Material.GOLDEN_APPLE, 1);
			// give item
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat, eat2);
		}
		return true;
	}
}
