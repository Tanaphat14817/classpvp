package me.tanaphat.clas;

import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Impaler implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -277, 72, 225);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 99999, 0));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(0, 255, 255));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(0, 255, 255));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", 7, Operation.ADD_NUMBER);
			ItemStack hand = new ItemStack(Material.TRIDENT);
			hand.addEnchantment(Enchantment.LOYALTY, 1);
			ItemMeta le = hand.getItemMeta();
			le.setUnbreakable(true);
			le.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
			hand.setItemMeta(le);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);

			player.getInventory().addItem(hand, eat);
		}
		return true;
	}
}