package me.tanaphat.clas;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Ace implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -303, 201, 194);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(0, 0, 0));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.ELYTRA);
			ItemMeta be = body.getItemMeta();
			be.setUnbreakable(true);
			body.setItemMeta(be);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(41, 51, 0));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(41, 51, 0));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
			ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
			ItemMeta e = leg.getItemMeta();
			e.setUnbreakable(true);
			hand.setItemMeta(e);
			ItemStack p1 = new ItemStack(Material.SPLASH_POTION, 15);
			PotionMeta pm = (PotionMeta) p1.getItemMeta();
			pm.setMainEffect(PotionEffectType.HARM);
			PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 20, 1);
			pm.addCustomEffect(harm, true);
			pm.setDisplayName("�4Carpet Bomb");
			p1.setItemMeta(pm);
			ItemStack p2 = new ItemStack(Material.LINGERING_POTION, 5);
			PotionMeta pm2 = (PotionMeta) p2.getItemMeta();
			pm2.setMainEffect(PotionEffectType.HARM);
			PotionEffect poi = new PotionEffect(PotionEffectType.HARM, 200, 2);
			pm2.addCustomEffect(poi, true);
			pm2.setDisplayName("�cMOLOTOV");
			p2.setItemMeta(pm2);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat, p1, p2);
		}
		return true;
	}
}
