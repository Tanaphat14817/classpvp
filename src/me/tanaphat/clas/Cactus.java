package me.tanaphat.clas;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Cactus implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -253, 73, 196);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			// item here
			ItemStack head = new ItemStack(Material.IRON_HELMET);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			body.addUnsafeEnchantment(Enchantment.THORNS, 14);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.GREEN);
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			leg.addUnsafeEnchantment(Enchantment.THORNS, 14);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.GREEN);
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			foot.addUnsafeEnchantment(Enchantment.THORNS, 14);
			LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
			foo.setColor(Color.GREEN);
			foo.setUnbreakable(true);
			foot.setItemMeta(foo);
			ItemStack hand = new ItemStack(Material.STONE_SWORD);
			ItemMeta he = hand.getItemMeta();
			he.setUnbreakable(true);
			hand.setItemMeta(he);
			ItemStack hand2 = new ItemStack(Material.SHIELD);
			hand2.addUnsafeEnchantment(Enchantment.THORNS, 10);
			hand2.setDurability((short) 334);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, hand2, eat);
		}
		return true;
	}
}
