package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Confuser implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();

			Location fix = new Location(player.getWorld(), -337, 46, 256);

			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.RED_MUSHROOM_BLOCK);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(204, 0, 153));
			bod.setUnbreakable(true);
			body.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(255, 120, 0));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.IRON_BOOTS);
			ItemStack hand = new ItemStack(Material.STONE_SWORD);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
			ItemStack sus = new ItemStack(Material.SUSPICIOUS_STEW, 3);
			ItemMeta su = sus.getItemMeta();
			su.setDisplayName(ChatColor.LIGHT_PURPLE + "Confusing Stew");
			sus.setItemMeta(su);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat, sus);
		}
		return true;
	}
}