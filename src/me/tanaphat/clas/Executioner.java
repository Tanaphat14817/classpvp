package me.tanaphat.clas;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Executioner implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -354, 74, 194);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 999999, 2));
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			player.teleport(back);

			// item here
			ItemStack head = new ItemStack(Material.BLACK_WOOL);
			ItemStack leg = new ItemStack(Material.IRON_LEGGINGS);
			ItemMeta le = leg.getItemMeta();
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
			ItemStack hand = new ItemStack(Material.IRON_AXE);
			ItemMeta l = hand.getItemMeta();
			l.setUnbreakable(true);
			hand.setItemMeta(l);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat);
		}
		return true;
	}
}