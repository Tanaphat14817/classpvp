package me.tanaphat.clas;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Assassin implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 1));

			Location fix = new Location(player.getWorld(), -345, 74, 242);

			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(0, 0, 0));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(0, 0, 0));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(0, 0, 0));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
			fo.setColor(Color.fromRGB(0, 0, 0));
			fo.setUnbreakable(true);
			foot.setItemMeta(fo);
			ItemStack hand = new ItemStack(Material.WOODEN_SWORD);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
			AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", 10, Operation.ADD_NUMBER);
			ItemStack Ani = new ItemStack(Material.GOLDEN_SWORD);
			ItemMeta meleemeta = Ani.getItemMeta();
			meleemeta.setDisplayName(ChatColor.BLUE + "Orphan Obliterator");
			meleemeta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
			Ani.setItemMeta(meleemeta);
			Ani.setDurability((short) 31);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);

			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, Ani, eat);
		}
		return true;
	}
}