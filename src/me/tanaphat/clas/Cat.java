package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Cat implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			player.getInventory().clear();			
			Location back = new Location(player.getWorld(), -290, 74, 214);
			
			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,999999,2));
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
			
			//item here

			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
				bod.setColor(Color.fromRGB(255, 165, 0));
				bod.setUnbreakable(true);
				body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
				le.setColor(Color.fromRGB(255, 165, 0));
				le.setUnbreakable(true);
				leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta fo = (LeatherArmorMeta) foot.getItemMeta();
				fo.setColor(Color.fromRGB(255, 165, 0));
				fo.setUnbreakable(true);
				foot.setItemMeta(fo);
			
			foot.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 6);
			ItemStack melee = new ItemStack(Material.PUMPKIN_SEEDS);
				ItemMeta meleemeta = melee.getItemMeta();
				meleemeta.setDisplayName(ChatColor.YELLOW + "Cat's claw");
				melee.setItemMeta(meleemeta);
			melee.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
				ItemMeta he = melee.getItemMeta();
				he.setUnbreakable(true);
				melee.setItemMeta(he);
			ItemStack eat = new ItemStack(Material.COOKED_COD, 16);
			//give item

			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(melee, eat);
		}
        return true;
    }
}