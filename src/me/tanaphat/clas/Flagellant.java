package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Flagellant implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();

			Location fix = new Location(player.getWorld(), -303, 73, 247);

			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 99999, 0));
			// item here
			ItemStack head = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta he = (LeatherArmorMeta) head.getItemMeta();
			he.setColor(Color.fromRGB(110, 0, 0));
			he.setUnbreakable(true);
			head.setItemMeta(he);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(100, 0, 0));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);

			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(110, 0, 0));
			le.setUnbreakable(true);
			leg.setItemMeta(le);

			ItemStack foot = new ItemStack(Material.CHAINMAIL_BOOTS);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 12);
			ItemStack melee = new ItemStack(Material.LEAD);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.RED + "Blood sloaked whip");
			melee.setItemMeta(meleemeta);
			melee.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
			ItemStack pain = new ItemStack(Material.RED_DYE, 3);
			ItemMeta meleemet = pain.getItemMeta();
			meleemet.setDisplayName(ChatColor.RED + "PAIN FOR GAIN!");
			pain.setItemMeta(meleemet);
			ItemStack rest = new ItemStack(Material.GREEN_DYE, 3);
			ItemMeta meleeme = rest.getItemMeta();
			meleeme.setDisplayName(ChatColor.GREEN + "*Rest the mind*");
			rest.setItemMeta(meleeme);
			ItemStack re = new ItemStack(Material.GRAY_DYE, 1);
			ItemMeta meleem = re.getItemMeta();
			meleem.setDisplayName("REPLENISH");
			re.setItemMeta(meleem);
			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(melee, eat, pain, rest, re);
		}
		return true;
	}

}
