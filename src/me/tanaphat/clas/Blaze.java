package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Blaze implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();

			Location fix = new Location(player.getWorld(), -246, 77, 143);

			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 999999, 3));
			player.teleport(fix);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(142, 101, 39));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(251, 172, 45));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
			foo.setColor(Color.fromRGB(251, 172, 45));
			foo.setUnbreakable(true);
			foot.setItemMeta(foo);
			ItemStack hand = new ItemStack(Material.BOW);
			hand.addEnchantment(Enchantment.ARROW_FIRE, 1);
			ItemMeta h = hand.getItemMeta();
			h.setUnbreakable(true);
			hand.setItemMeta(h);
			ItemStack arrow = new ItemStack(Material.ARROW, 15);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			ItemStack melee = new ItemStack(Material.BLAZE_ROD);
			ItemMeta meleemeta = melee.getItemMeta();
			meleemeta.setDisplayName(ChatColor.RED + "HOT ROD");
			melee.setItemMeta(meleemeta);
			ItemStack floa = new ItemStack(Material.FEATHER);
			ItemMeta meleemet = floa.getItemMeta();
			meleemet.setDisplayName(ChatColor.WHITE + "Float");
			floa.setItemMeta(meleemet);

			melee.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
			// give item
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, melee, eat, arrow, floa);
		}
		return true;
	}
}
