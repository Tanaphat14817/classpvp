package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Goat implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();

			Location fix = new Location(player.getWorld(), -313, 88, 146);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));

			player.teleport(fix);
			// item here
			ItemStack head = new ItemStack(Material.GRAY_WOOL);
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
			bod.setColor(Color.fromRGB(255, 255, 255));
			bod.setUnbreakable(true);
			body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
			le.setColor(Color.fromRGB(255, 255, 255));
			le.setUnbreakable(true);
			leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta foo = (LeatherArmorMeta) foot.getItemMeta();
			foo.setColor(Color.fromRGB(255, 255, 255));
			foo.setUnbreakable(true);
			foot.setItemMeta(foo);
			ItemStack hand = new ItemStack(Material.DIORITE);
			hand.addUnsafeEnchantment(Enchantment.KNOCKBACK, 3);
			hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
			ItemMeta meleemeta = hand.getItemMeta();
			meleemeta.setDisplayName(ChatColor.WHITE + "RAM!");
			hand.setItemMeta(meleemeta);
			ItemStack hand2 = new ItemStack(Material.WOODEN_SWORD);
			hand2.addUnsafeEnchantment(Enchantment.KNOCKBACK, 12);
			ItemMeta meleemeta2 = hand2.getItemMeta();
			meleemeta2.setDisplayName(ChatColor.GREEN + "GOOGLE CHROME");
			hand2.setItemMeta(meleemeta2);
			hand2.setDurability((short) 58);
			ItemStack eat = new ItemStack(Material.BREAD, 16);

			// give item
			player.getInventory().setHelmet(head);
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, hand2, eat);
		}
		return true;
	}
}
