package me.tanaphat.clas;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Stalker implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			player.getInventory().clear();
			Location back = new Location(player.getWorld(), -340, 73, 167);

			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100));
			// item here

			ItemStack hand = new ItemStack(Material.IRON_SWORD);
			ItemStack eat = new ItemStack(Material.COOKED_BEEF, 8);
			// give item
			player.getInventory().addItem(hand, eat);
		}
		return true;
	}
}