package me.tanaphat.clas;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Shark implements CommandExecutor,Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			player.getInventory().clear();			
			Location back = new Location(player.getWorld(), -322, 69, 252);
			
			player.teleport(back);
			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL,20,100));
			player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING,999999,100));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING,999999,1));
			//item here
			ItemStack body = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta bod = (LeatherArmorMeta) body.getItemMeta();
				bod.setColor(Color.fromRGB(0, 0, 93));
				bod.setUnbreakable(true);
				body.setItemMeta(bod);
			ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta le = (LeatherArmorMeta) leg.getItemMeta();
				le.setColor(Color.fromRGB(0, 0, 93));
				le.setUnbreakable(true);
				leg.setItemMeta(le);
			ItemStack foot = new ItemStack(Material.IRON_BOOTS);
				ItemMeta l = foot.getItemMeta();
				l.setUnbreakable(true);
				foot.setItemMeta(l);
				foot.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 4);
			ItemStack hand = new ItemStack(Material.IRON_NUGGET);
			 ItemMeta handM = hand.getItemMeta();
			 handM.setDisplayName(ChatColor.AQUA + "Shark's Tooth");
			 hand.addEnchantment(Enchantment.DAMAGE_ALL, 2);
			ItemStack eat = new ItemStack(Material.COOKED_SALMON, 20);
			//give item
			player.getInventory().setChestplate(body);
			player.getInventory().setLeggings(leg);
			player.getInventory().setBoots(foot);
			player.getInventory().addItem(hand, eat);
		}
        return true;

	}
}