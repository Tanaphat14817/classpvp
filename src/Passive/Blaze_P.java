package Passive;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.tanaphat.inv.Menu;

public class Blaze_P implements Listener{
	@EventHandler
	public void blazemove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		Block b = p.getLocation().getBlock();
		if (Menu.seleceted.get(p).equals("Blaze")) {
			if (b.getType().equals(Material.WATER)) {
				p.damage(5);
			}
		}
	}
	@EventHandler
	public void blazemove2(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		Block b = p.getLocation().getBlock();
		if (Menu.seleceted.get(p).equals("Blaze")) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE,1));
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.MAX_VALUE,3));
			}
		}
	
}

